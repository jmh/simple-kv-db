module kv

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/client_golang v1.12.0
	//github.com/dijit/concurrent-hashmap v0.0.0
)

//replace (
//	"github.com/dijit/concurrent-hashmap" => "./concurrent-hashmap"
//	"github.com/dijit/handlers" => "./handlers"
//)
//