package kv

import (
	b64 "encoding/base64"
	fmt "fmt"
	gmux "github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	cmap "kv/concurrent-hashmap"
	"net/http"
	"strconv"
	"time"
)

var (
	HistogramTimeSet = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "app",
			Name:      "time_to_set",
			Help:      "How long it took to set",
			Buckets: []float64{0.00125, 0.00130, 0.00135, 0.00145, 0.0016, 0.0017, 0.0019, 0.011, 0.015},
		}, []string{"statuscode"})
	HistogramTimeGet = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "app",
			Name:      "time_to_get",
			Help:      "How long it took to get",
			Buckets: []float64{0.00125, 0.00130, 0.00135, 0.00145, 0.0016, 0.0017, 0.0019, 0.011, 0.015},
		}, []string{"statuscode"})
)

// In-memory key value store
var store = cmap.New()

// get the complete store (for testing purposes)
var GetAll = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	storeJson, err := store.MarshalJSON()
	if err != nil { }
	_, err = fmt.Fprintln(w, string(storeJson))
	if err != nil { }
}

// get all the active keys (this is nicer than GetAll which is really better for testing)
var GetAllKeys = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "plain/text")
	keys := store.Keys()
	_, err := fmt.Fprintln(w, keys)
	if err != nil { }
}

// get value of a key
var Get = func(w http.ResponseWriter, r *http.Request) {
	timeStart := time.Now()
	status := http.StatusOK
	defer func() {
		HistogramTimeGet.WithLabelValues(fmt.Sprintf("%d", status)).Observe(time.Since(timeStart).Seconds())
	}()
	w.Header().Set("Content-Type", "text/plain")
	// get key from request, output value
	vars := gmux.Vars(r)
	tmp, exists := store.Get(vars["key"])
	if !exists {
		status = http.StatusNotFound
		http.Error(w, "Error: Key Not Found", status)
		return
	}
	if tmp == nil {
		status = http.StatusNoContent
		http.Error(w, "ERROR: Value is empty", status)
		return
	}
	ret, err := b64.StdEncoding.DecodeString(tmp.(string))
	if err != nil {
		status = http.StatusNotFound
		http.Error(w, "ERROR: Failed to decode b64 contents, memory corruption", status)
		return
	}
	_, err = fmt.Fprintf(w, string(ret))
	if err != nil { }
}

// set keys and values
var Set = func(w http.ResponseWriter, r *http.Request) {
	timeStart := time.Now()

	/*
	randy := rand.Intn(10)
	time.Sleep(time.Duration(randy) * time.Millisecond)
	 */

	// We override PartialContent when we hit EOF of the BODY, if we never reach it then we know we don't have all data
	status := http.StatusPartialContent
	// It's nice to record statustics
	defer func() {
		HistogramTimeSet.WithLabelValues(fmt.Sprintf("%d", status)).Observe(time.Since(timeStart).Seconds())
	}()
	if r.Header.Get("Content-Type")  == "application/json" {
		// I thought it might be fun to unmarshal some JSON into a bulk loading of keys/values.
		// But this broke the principle of least surprise.
		// Leaving it as "Not Implemented" in case I come back to it.
		status = http.StatusNotImplemented
		w.WriteHeader(status)
		return
	}
	w.Header().Set("Content-Type", "text/plain")
	size, err := strconv.Atoi(r.Header.Get("Content-Length"))
	if err != nil {
		size = 100_000 // max POST size is 100k if I can't read the header (but IE/Firefox have 2GiB and Chrome/Opera has 4GB)
	}
	vars := gmux.Vars(r)
	buf := make([]byte, int32(size))
	numberOfBytes, err := r.Body.Read(buf)
	if err != nil {
		if err.Error() == "EOF" {
			// this is expected, if we reach EOF we know we have all the content.
			status = http.StatusOK
		} else {
			status = http.StatusInternalServerError
		}
	}
	//fmt.Printf("[info] Key %s Content size is: %d\n", vars["key"], numberOfBytes)
	encodedContent := b64.StdEncoding.EncodeToString(buf[:numberOfBytes])
	store.Set(vars["key"], encodedContent)
	w.WriteHeader(status)
}