package kv

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
)

// Test for "Set" API (POST method)
func TestCheckSetHandler(t *testing.T) {
	// Create request to pass to handler (serves as "r *http.Request" in the test)
	data := strings.NewReader(`{"abc-1":"one"}`)
	req, err := http.NewRequest("POST", "/crap", data)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "text/plain")

	// Create a ResponseRecorder to record the response.
	// (This will serve as the "w http.ResponseWriter" in the test.)
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Set)

	// Handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `{"abc-1":"one"}`
	// Length of actual response was more than expected
	if strings.TrimSpace(rr.Body.String()) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

// Test for "Get" API
func TestCheckGetHandler(t *testing.T) {
	// send set request to initialize kv store
	sendingData := `{"abc-1":1,"abc-2":2,"xyz-1":"three","xyz-2":4}`
	data := strings.NewReader(sendingData)
	reqSend, err := http.NewRequest("POST", "/fings", data)
	if err != nil {
		t.Fatal(err)
	}
	reqSend.Header.Set("Content-Type", "text/plain")
	rr := httptest.NewRecorder()
	//handler := http.HandlerFunc(Set)
	//handler.ServeHTTP(rr, reqSend)

	// set up and execute test cases
	reqReceive, err := http.NewRequest("GET", "/fings", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	// Need to create a router that we can pass the request through so that the vars will be added to the context
	r := mux.NewRouter()
	r.HandleFunc("/{key}", Get)
	r.ServeHTTP(rr, reqSend)
	r.ServeHTTP(rr, reqReceive)

	// If test should pass, but it fails
	if rr.Code != http.StatusOK && true {
		t.Errorf("handler returned wrong status code: got %v want %v", rr.Code, http.StatusOK)
	}

	// Compare expected and received values
	expected := fmt.Sprintf("%v", sendingData)
	received := fmt.Sprintf("%v", strings.Trim(strings.TrimSpace(rr.Body.String()), "\""))
	if received != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", received, expected)
	}
}

// Test for "GetAll" API
/* FIXME: this is when we support JSON
func TestCheckGetAllHandler(t *testing.T) {
	// send set request to initialize kv store
	data := strings.NewReader(`{"abc-1":1,"abc-2":2,"xyz-1":"three","xyz-2":4}`)
	req, err := http.NewRequest("POST", "/", data)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	setHandler := http.HandlerFunc(Set)
	setHandler.ServeHTTP(rr, req)

	// send get request at "/" endpoint for all keys and values
	req, err = http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	getHandler := http.HandlerFunc(GetAll)
	getHandler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	// compare expected and received body
	expected := `{"abc-1":1,"abc-2":2,"xyz-1":"three","xyz-2":4}`
	if strings.TrimSpace(rr.Body.String()) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}
*/

func TestCheckGetAllHandler(t *testing.T) {
	// send set request to initialize kv store
	data := strings.NewReader(`SomeKindOfCrap`)
	req, err := http.NewRequest("POST", "/SomeCrapKey", data)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "text/plain")
	rr := httptest.NewRecorder()
	setHandler := http.HandlerFunc(Set)
	setHandler.ServeHTTP(rr, req)

	// send get request at "/" endpoint for all keys and values
	req, err = http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	getHandler := http.HandlerFunc(GetAllKeys)
	getHandler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	// compare expected and received body
	expected := `[SomeCrapKey]`
	if strings.TrimSpace(rr.Body.String()) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}
