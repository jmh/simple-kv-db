# Image that we'll use to build our static binary (Golang is staticly compiled)
FROM golang:1.17-bullseye as build

WORKDIR /go/src/app
COPY *.go /go/src/app
# Move our fancy imports to GOPATH
COPY concurrent-hashmap /usr/local/go/src/kv/concurrent-hashmap
COPY handlers /usr/local/go/src/kv/handlers

RUN go mod init && \
    go mod tidy && \
    go get -d -v ./...

RUN CGO_ENABLED=0 go build -o /go/bin/kvstore

# Image that we'll use to run; we could use scratch but the distroless
FROM gcr.io/distroless/static
COPY --from=build /go/bin/kvstore /
EXPOSE 8080
EXPOSE 2112
CMD ["/kvstore"]