This is an in-memory key-value store HTTP API service, with the following endpoints:

- `/{key}` : GET method. Returns the value of a previously set key.

- `/{key}` : POST method. Send in a value to be stored at a given key

- `/` : GET method. Returns all the keys in the in-memory store. Also useful for readiness probes and load testing. 

## Architecture

`./concurrent-hashmap` contains a concurrent-hashmap implementation which is sharded into 32 hashmaps.
the {key} which is provided is run through the hashing function to determine which hashmap shard should be used.
The hashmaps are multi-read, single write (locking provided by [sync.RWMutex](https://pkg.go.dev/sync#RWMutex))

`./handlers` contains the helper functions called by main.go to handle requests, including some (duplicated) prometheus metrics.
As such, `./handlers` depends on concurrent-hashmap

## How to run (as binary / without bazel)

- Run `go build -o kvstore`.
- Run the `./kvstore` binary (the service will run on port 8080). 

(Alternatively, you can simply run `go build` and run the resulting `./kv` binary, or give another name to it.)

## How to run (as binary / with bazel)

- Run `bazel build //:kv_binary`
- Run the `./bazel-bin/kv_binary_/kv_binary` binary

(you can of course use `bazel run //:kv_binary`)

## How to build and use Docker image (DockerFile)

- Build the Docker image with the command

```
docker build -t <image-name>:<tag-name> .
```

For example, you can run `docker build -t kv:latest .`

- After building the image, run it with the command
```
docker run -d -p 8080:8080 -p 2112:2112 <image-name>
```
(The service will run on port 8080 in this case.)

## Running the service on a k8s cluster

- Run `kubectl apply -f deploy.yaml`.
- Run `kubectl apply -f deploy.prometheus.access.yaml`.

### Testing 

#### Basic

We can insert 4,000 keys concurrently 
* **NOTE**: Time is only going to give us _relative_ results, this is not accurate for load testing.
* **NOTE**: The server seems to be able to support much more than this, but my laptop cannot create enough curl processes to saturate it.
* **NOTE**: Getting the output of `time` is awkward due to the job control spam in the terminal

##### Sets:
```bash
for x in $(seq 0 2000);
do
    curl -d '333333Things🤨' localhost:8080/thing${x} &
done
for x in $(seq 0 2000);
do
    curl -d '333333Things🤨' localhost:8080/unthing${x} &
done
time wait
```
on my machine:
```tsv
real	0m0.112s
user	0m0.502s
sys     0m0.820s
```

##### Gets:
```bash
for x in $(seq 0 2000);
do
    curl -qs localhost:8080/thing${x} >/dev/null &
done
time for x in $(seq 0 2000);
do
    curl -qs localhost:8080/unthing${x} >/dev/null &
done
time wait
```

on my machine:
```tsv
real	0m0.017s
user	0m0.021s
sys	    0m0.030s
```


#### Load

I've done basic load testing with the tool [Fortio](https://fortio.org/) in the following way:
```
fortio load -a -c 50 -qps 500 -t 180s "localhost:8080"
```

Status 200 response was obtained for 100% of the requests. 

### Known issues

- Replicas do not have a shared memory, and so have different key-value pair stores, instead of all replicas utilizing a common store.
- Key-value pairs are not stored anywhere outside the pod, thus, while requests can be made consistently without downtime, any stored values are lost when a pod restarts or goes down.
- Memory is only free'd on GET of an expired resource. If there is no GET requests against a specific resource then the key is still listed as "active" in `/`
