package main

import (
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	cmap "kv/concurrent-hashmap"
	"kv/handlers"
	"log"
	"net/http"
)

var (
	RequestDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "app",
		Name:      "request_duration_seconds",
		Help:      "Time (in seconds) spent serving HTTP requests.",
		Buckets:   []float64{0.0000125, 0.00025, 0.00035, 0.00045, 0.006, 0.007, 0.009, 0.01, 0.05},
	}, []string{"method"})
)

func init() {
	prometheus.MustRegister(RequestDuration)
	prometheus.MustRegister(cmap.GaugeTotalEntries)
	prometheus.MustRegister(kv.HistogramTimeSet)
	prometheus.MustRegister(kv.HistogramTimeGet)
}

func prometheusMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//TODO: Find a way to find status codes
		//route := mux.CurrentRoute(r)
		timer := prometheus.NewTimer(RequestDuration.WithLabelValues(r.Method))
		next.ServeHTTP(w, r)
		timer.ObserveDuration()
	})
}

func main() {
	// First we initialise our healthcheck port
	health := mux.NewRouter()
	health.Handle("/metrics", promhttp.Handler())
	log.Println("Prometheus export listening on port :2112/metrics")
	go http.ListenAndServe(":2112", health)

	// Then we initialise our Application port
	r := mux.NewRouter()
	r.Use(prometheusMiddleware)
	r.HandleFunc("/{key}", kv.Get).Methods("GET")
	r.HandleFunc("/{key}", kv.Set).Methods("POST")
	r.HandleFunc("/", kv.GetAllKeys).Methods("GET")

	log.Println("App listening on port :8080")
	log.Fatal(http.ListenAndServe(":8080", r))
}
