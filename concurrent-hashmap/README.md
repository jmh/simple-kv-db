# Yes This is the code you're looking for

I decided that implementing a basic [concurrent hashmap](https://www.geeksforgeeks.org/concurrenthashmap-in-java/) would be better for a heavy update/read rate.

As explained [here](http://golang.org/doc/faq#atomic_maps) and [here](http://blog.golang.org/go-maps-in-action), the `map` type in Go doesn't support concurrent reads and writes. `concurrent-hashmap` provides a high-performance solution to this by sharding the map with minimal time spent waiting for locks.

Prior to Go 1.9, there was no concurrent map implementation in the stdlib. In Go 1.9, `sync.Map` was introduced. The new `sync.Map` has a few key differences from this map. The stdlib `sync.Map` is designed for append-only scenarios. So if you want to use the map for something more like in-memory db, you might benefit from doing it like this. 

You can read more about it in the golang repo, for example [here](https://github.com/golang/go/issues/21035) and [here](https://stackoverflow.com/questions/11063473/map-with-concurrent-access)

## usage

Import the package:

```go
import (
	"kv/concurrent-hashmap"
)

```

The package will be imported under the "cmap" namespace.

## example

```go

	// Create a new map.
	m := cmap.New()

	// Sets item within map, sets "bar" under key "foo"
	m.Set("foo", "bar")

	// Retrieve item from map.
	if tmp, ok := m.Get("foo"); ok {
		bar := tmp.(string)
	}

	// Removes item under key "foo"
	m.Remove("foo")

```

For more examples have a look at concurrent_map_test.go.

Running tests:

```bash
go test "kv/concurrent-hashmap"
```
